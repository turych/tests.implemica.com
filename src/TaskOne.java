import java.util.Scanner;

/**
 * Find the number of regular bracketed expressions,
 * containing N opening and N closing brackets
 *
 * Resolve the task by Catalan number.
 * See more on wiki:
 * http://ru.gowikipedia.org/wiki/%D0%A7%D0%B8%D1%81%D0%BB%D0%B0_%D0%9A%D0%B0%D1%82%D0%B0%D0%BB%D0%B0%D0%BD%D0%B0
 */

public class TaskOne {

    public static void main(String[] args) {
        // input from console
        Scanner input = new Scanner(System.in);
        int N = input.nextInt();
        // print result to console
        System.out.print(findCatalanNumber(N));
    }

    /**
     * Find Catalan number for N.
     *
     * Function use simple recurrence relations.
     * See more ./resources/simple_recurrence_relations.svg
     */
    public static int findCatalanNumber(int n) {

        float result;

        // the zero number Catalana - is 1
        if (n == 0)
            return 1;

        result = 2f*(2*n - 1)/(n + 1)*findCatalanNumber(n - 1);
        return (int) result;
    }
}
