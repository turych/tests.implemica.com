import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

public class TaskTwo {

    public static double INF = Double.POSITIVE_INFINITY;

    public static void main(String[] args) {

        try(BufferedReader reader = Files.newBufferedReader(
                Paths.get("./resources/input_task_two.txt"), StandardCharsets.UTF_8
        )) {
            // read count of tests
            String line = reader.readLine();
            int testsCount = Integer.valueOf(line);

            // Run tests
            for (int testIndex = 0; testIndex < testsCount; testIndex++) {
                // read count of cities
                int countCities = Integer.valueOf(reader.readLine());
                // Create list of cities.
                HashMap<String, Integer> cities = new HashMap<>();
                // Create matrix(graph) with costs.
                Matrix matrix = new Matrix(countCities);

                for (int cityIndex = 0; cityIndex < countCities; cityIndex++) {
                    String cityName = reader.readLine();
                    cities.put(cityName, cityIndex);
                    // read count of neighbors of city
                    int countNbs = Integer.valueOf(reader.readLine());
                    for (int i = 0; i < countNbs; i++) {
                        line = reader.readLine();
                        String[] parsedLine = line.split(" ");
                        // write to matrix neighbors & costs
                        // parsedLine[0] - index of a city connected to NAME
                        // parsedLine[1] - the transportation cost
                        matrix.setCost(cityIndex, Integer.valueOf(parsedLine[0]) - 1, Double.valueOf(parsedLine[1]));
                    }
                }

                matrix.findCheapest();

                // read the number of paths to find
                int numberPaths = Integer.valueOf(reader.readLine());
                // read paths(source, destination)
                for (int pathIndex = 0; pathIndex < numberPaths; pathIndex++) {
                    line = reader.readLine();
                    String[] parsedLine = line.split(" ");
                    int source = cities.get(parsedLine[0]);
                    int destination = cities.get(parsedLine[1]);
                    // Print minimum transportation cost from source to destination
                    System.out.println((int) matrix.getCost(source, destination));
                }

                System.out.println();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
