import java.math.BigInteger;

public class TaskThree {

    public static void main(String[] args) {

        // search factorial
        BigInteger factorial = factorial(100);

        // convert BigInteger to string
        String str = factorial.toString();

        // find the sum of the all digits
        int summ = 0;
        for (int i = 0; i < str.length(); i++) {
            summ += Character.getNumericValue(str.charAt(i));
        }

        System.out.println(summ);
    }

    /**
     * Search the factorial of a number.
     *
     * @param n
     * @return BigInteger
     */
    static BigInteger factorial(int n) {
        BigInteger result = BigInteger.valueOf(1L);
        for (int i = 1; i <= n; i++) {
            result = result.multiply(BigInteger.valueOf(i));
        }
        return result;
    }
}
