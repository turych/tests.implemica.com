import static java.lang.Math.min;

public class Matrix {

    private static double INF = Double.POSITIVE_INFINITY;
    private double[][] matrix;

    public Matrix(int size) {
        matrix = new double[size][size];
        initMatrix();
    }

    /**
     * Set cost from source to destination
     *
     * @param from  source index
     * @param to    destination index
     * @param cost
     */
    public void setCost(int from, int to, double cost) {
        matrix[from][to] = cost;
    }

    /**
     * Get cost from source to destination
     *
     * @param from  source index
     * @param to    destination index
     * @return
     */
    public double getCost(int from, int to) {
        return matrix[from][to];
    }

    /**
     * Apply Floyd-Warshall algorithm to matrix for find the minimum transportation costs.
     */
    public void findCheapest() {
        floydWarshall();
    }

    /**
     * Populating a matrix with empty data
     */
    private void initMatrix() {
        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix.length; k++) {
                if (i == k) {
                    matrix[i][k] = 0;
                } else {
                    matrix[i][k] = INF;
                }
            }
        }
    }

    private void floydWarshall() {

        int matrixLength = matrix.length;

        for (int k = 0; k < matrixLength; ++k) {
            for (int i = 0; i < matrixLength; ++i) {
                for (int j = 0; j < matrixLength; ++j) {
                    matrix[i][j] = min(matrix[i][j], matrix[i][k] + matrix[k][j]);
                }
            }
        }
    }
}
